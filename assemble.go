// Package assembler provides an assembler for LC-3 architecture.
package assembler

import (
	"fmt"
	"strings"
)

const (
	Bin  = 0
	Hex  = 1
	Addr = 2
)

// Segment is a segment of machine code,
// starting with ".orig" and ending with ".end".
type Segment struct {
	Start uint16
	Inst  []uint16
}

// ToString converts a Segment into string.
// Flags Bin, Hex and Addr are available.
// If Bin and Hex are used at the same time,
// ToString will output hexadecimal string.
// Bin and Hex control only the radix of program,
// and address is always in hexadecimal radix.
func (s Segment) ToString(flag int) string {
	var sb strings.Builder
	for i, v := range s.Inst {
		if flag&Addr != 0 {
			sb.WriteString(fmt.Sprintf("x%04X  ", s.Start+uint16(i)))
		}
		if flag&Hex != 0 {
			sb.WriteString(fmt.Sprintf("%04X\n", v))
		} else {
			sb.WriteString(fmt.Sprintf("%016b\n", v))
		}
	}
	return sb.String()
}

// Assemble converts assembly code into machine code.
func Assemble(src []byte) ([]Segment, error) {
	seg, err := parse(src)
	if err != nil {
		return nil, err
	}

	Seg := make([]Segment, len(seg))
	for i := range Seg {
		Seg[i].Start = seg[i].start
		Seg[i].Inst = make([]uint16, len(seg[i].inst))
		for j := range Seg[i].Inst {
			Seg[i].Inst[j] = seg[i].inst[j].code
		}
	}
	return Seg, nil
}
