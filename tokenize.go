package assembler

import (
	"fmt"
	"strconv"
	"strings"
)

type tokenType uint8

const (
	tAdd tokenType = iota
	tAnd
	tBr
	tJmp
	tJsr
	tJsrr
	tLd
	tLdi
	tLdr
	tLea
	tNot
	tRet
	tRti
	tSt
	tSti
	tStr
	tTrap
	tGetc
	tOut
	tPuts
	tIn
	tPutsp
	tHalt

	tOrig
	tEnd
	tFill
	tBlkw
	tStringz

	tLabel
	tReg
	tNumber
	tString
	tComma
	tNewline

	tEndOfFile
)

var keywords = map[string]tokenType{
	"add": tAdd, "and": tAnd, "br": tBr, "jmp": tJmp, "jsr": tJsr, "jsrr": tJsrr, "ld": tLd, "ldi": tLdi, "ldr": tLdr,
	"lea": tLea, "not": tNot, "ret": tRet, "rti": tRti, "st": tSt, "sti": tSti, "str": tStr, "trap": tTrap,
	"getc": tGetc, "out": tOut, "puts": tPuts, "in": tIn, "putsp": tPutsp, "halt": tHalt, ".orig": tOrig, ".end": tEnd,
	".fill": tFill, ".blkw": tBlkw, ".stringz": tStringz,
}

type token struct {
	kind tokenType
	raw  []byte
	str  string
	val  uint16
}

func tokenize(src []byte) ([]*token, error) {
	var tokens []*token
	for lo := 0; lo < len(src); {
		hi := lo + 1
		switch ch := src[lo]; true {
		default:
			return nil, fmt.Errorf("unknown token %s", src[lo:lo+1])
		case isAlpha(ch):
			for hi < len(src) && isAlphaOrNum(src[hi]) {
				hi++
			}
			if ok, val := parseHexInt(src[lo:hi]); ok {
				tokens = append(tokens, &token{tNumber, src[lo:hi], "", val})
			} else if ok, val = parseReg(src[lo:hi]); ok {
				tokens = append(tokens, &token{tReg, src[lo:hi], "", val})
			} else if ok, val = parseBr(src[lo:hi]); ok {
				tokens = append(tokens, &token{tBr, src[lo:hi], "", val})
			} else {
				str := strings.ToLower(string(src[lo:hi]))
				if kind, ok := keywords[str]; ok {
					tokens = append(tokens, &token{kind, src[lo:hi], "", 0})
				} else {
					tokens = append(tokens, &token{tLabel, src[lo:hi], str, 0})
				}
			}
		case ch == '.':
			for hi < len(src) && isAlpha(src[hi]) {
				hi++
			}
			str := strings.ToLower(string(src[lo:hi]))
			if kind, ok := keywords[str]; ok {
				tokens = append(tokens, &token{kind, src[lo:hi], "", 0})
			} else {
				return nil, fmt.Errorf("illegal pseudo-operator %s", src[lo:hi])
			}
		case ch == '#' || ch == '-' || ch == '+' || isDecNum(ch): // #10, #-10, 10, +10
			var dn, val int // digit number
			sign := +1

			if ch == '-' {
				sign = -1
			}
			if ch == '#' && hi < len(src) && (src[hi] == '+' || src[hi] == '-') {
				if src[hi] == '-' {
					sign = -1
				}
				hi++
			} else if isDecNum(ch) {
				dn++
				val = int(ch - '0')
			}

			for hi < len(src) && isDecNum(src[hi]) {
				val = val*10 + int(src[hi]-'0')
				dn++
				hi++
				if sign == +1 && val > 0x7fff || sign == -1 && val > 0x8000 {
					for hi < len(src) && isDecNum(src[hi]) {
						hi++
					}
					return nil, fmt.Errorf("too large integer for LC-3 %s", src[lo:hi])
				}
			}
			if dn == 0 {
				return nil, fmt.Errorf("illegal decimal integer %s", src[lo:hi])
			}
			tokens = append(tokens, &token{tNumber, src[lo:hi], "", uint16(val * sign)})
		case ch == '"':
			var escape int
			for hi < len(src) && !(escape%2 == 0 && src[hi] == ch) {
				if src[hi] == '\\' {
					escape++
				} else if src[hi] == '\n' {
					break
				} else {
					escape = 0
				}
				hi++
			}
			if hi == len(src) || src[hi] == '\n' { // src[hi] = '"'
				return nil, fmt.Errorf("unterminated string %s", src[lo:hi])
			}
			hi++
			raw := string(src[lo:hi])
			content, err := strconv.Unquote(raw)
			if err != nil {
				return nil, fmt.Errorf("illegal string %s", raw)
			}
			tokens = append(tokens, &token{tString, src[lo:hi], content, 0})
		case ch == '\'':
			if hi == len(src) || src[hi] == '\n' {
				return nil, fmt.Errorf("unterminated character %s", src[lo:hi])
			}
			if src[hi] == '\\' {
				if hi++; hi == len(src) || src[hi] == '\n' {
					return nil, fmt.Errorf("unterminated character %s", src[lo:hi])
				}
				if hi++; hi == len(src) || src[hi] != '\'' {
					return nil, fmt.Errorf("unterminated character %s", src[lo:hi])
				}
				hi++
			} else {
				if hi++; hi == len(src) || src[hi] != '\'' {
					return nil, fmt.Errorf("unterminated character %s", src[lo:hi])
				}
				hi++
			}
			content, err := strconv.Unquote("\"" + string(src[lo+1:hi-1]) + "\"")
			if err != nil {
				return nil, fmt.Errorf("illegal character %s", src[lo:hi])
			}
			tokens = append(tokens, &token{tNumber, src[lo:hi], "", uint16(content[0])})
		case ch == ',':
			tokens = append(tokens, &token{tComma, src[lo:hi], "", 0})
		case ch == '\n':
			tokens = append(tokens, &token{tNewline, src[lo:hi], "", 0})
		case isWhite(ch):
			for hi < len(src) && isWhite(src[hi]) {
				hi++
			}
		case ch == ';':
			for hi < len(src) && src[hi] != '\n' {
				hi++
			}
		}
		lo = hi
	}
	tokens = append(tokens, &token{tEndOfFile, nil, "", 0})
	return tokens, nil
}

func isAlpha(c byte) bool      { return 'a' <= c && c <= 'z' || 'A' <= c && c <= 'Z' || c == '_' }
func isDecNum(c byte) bool     { return '0' <= c && c <= '9' }
func isAlphaOrNum(c byte) bool { return isAlpha(c) || isDecNum(c) }
func isWhite(c byte) bool      { return c == ' ' || c == '\t' || c == '\r' }

func parseHexInt(src []byte) (bool, uint16) {
	if src[0] != 'x' {
		return false, 0
	}
	var result int
	for i := 1; i < len(src); i++ {
		if '0' <= src[i] && src[i] <= '9' {
			result = result*16 + int(src[i]-'0')
		} else if 'a' <= src[i] && src[i] <= 'f' {
			result = result*16 + int(src[i]-'a'+10)
		} else if 'A' <= src[i] && src[i] <= 'F' {
			result = result*16 + int(src[i]-'A'+10)
		} else {
			return false, 0
		}
		if result > 0xffff {
			return false, 0
		}
	}
	return true, uint16(result)
}
func parseReg(src []byte) (bool, uint16) {
	if len(src) != 2 || (src[0] != 'r' && src[0] != 'R') || src[1] < '0' || src[1] > '7' {
		return false, 0
	}
	return true, uint16(src[1] - '0')
}
func parseBr(src []byte) (bool, uint16) {
	if len(src) < 2 || len(src) > 5 ||
		(src[0] != 'b' && src[0] != 'B') ||
		(src[1] != 'r' && src[1] != 'R') {
		return false, 0
	}
	if len(src) == 2 {
		return true, 0b111
	}
	var result uint16
	visit := map[byte]bool{}
	for _, ch := range src[2:] {
		if !visit[ch] {
			switch ch {
			default:
				return false, 0
			case 'n', 'N':
				result |= 1 << 2
			case 'z', 'Z':
				result |= 1 << 1
			case 'p', 'P':
				result |= 1
			}
			if ch < 'a' {
				ch += 32
			}
			visit[ch] = true
		} else {
			return false, 0
		}
	}
	return true, result
}
