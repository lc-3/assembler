# LC-3 Assembler

Package assembler provides an assembler for LC-3 architecture.

## Case Sensitivity

This assembler is totally case-insensitive. In other words, `"ADD"`, `"add"` and `"aDd"` refer the same instruction, and `"loop"` and `"Loop"` are the same label. By the way, `"x30A0"` and `"X30a0"` are all acceptable.

## Definition of Tokens

Strings with a prefix `r` such as `r"a"` are regular expressions.

### Labels

```
[a-zA-Z_][0-9a-zA-Z_]*
```

`r"r[0-7]"` is reserved for registers, and `r"x[0-9a-f]{1,4}"` is occupied by hexadecimal integers. This indicates that `"r5"` is not a legal label while `"r8"` is. `"x44"` is not a legal label while `"x755ab"` is. `"r5"` will be interpreted as a register reference and `"x44"` will be interpreted as a hexadecimal integer. Misuse of this kind of labels may leads to unexpected problems. We suggest to avoid using label names starting with `"x"` and `"r"`.

### Keywords

Some labels are reserved for instructions, saying `"add"`, `"out"`, which cannot be used as label. `".fill"`, `".blkw"`, `".stringz"`, `".orig"`, `".end"` are also keywords. Other strings matching `r"\.[a-zA-Z_]*"` are illegal.

`"br"` is special. `r"br(n?z?p?|n?p?z?|z?n?p?|z?p?n?|p?n?z?|p?z?n?)"` are all keywords, and cannot be used as label.

### Numbers

```
x0*[0-9a-f]{1,4}|#?(+|-)?[0-9]+
```

Decimal integer starts with an optional `"#"`. Decimal integer smaller than -32768 or greater than 32767 leads to an error, while hexadecimal integer out of range turns to be a label.

### Strings and Characters

```
\"([^\"]|\\\")\"
'([^\\']|\\.)'
```

Strings are surrounded by double quotes where Unicode are available. Characters are surrounded by single quotes, where only ASCII characters are acceptable.

Note that character is just a special kind of number. `".orig 'a'"` is legal for tokenizer and parser.

### Commas and Newlines

`","` is optional, which indicates that `add r1,r1,r1` and `add r1 r1 r1` are both correct.

Newline is also a kind of token, ensuring that an instruction is completed in a single line. Label and related instruction can be written in different lines, but all parts of a instruction must be written in the same line.

## Grammar

Here is the grammar of LC-3 assembly language in EBNF form.

### Source File

```
SourceFile = MoreNewlines [ Segment { Newlines Segment } MoreNewlines ] .
```

Multiple segments of code are able to put into a single file, which means the following input is legal.

```
.orig x3000
; ...
.end

.orig x4000
; ...
.end
```

Note that label are not available in other segment.

### Segment

```
Segment = Orig { Inst } end .
Orig    = orig number Newlines .
```

Segment is a sequence of instructions surrounded by `".orig'` and `".end"`.

### Instruction

```
Inst = [ label MoreNewlines ]
       ( Add | And | Br  | Jmp | Jsr | Jsrr
       | Ld  | Ldi | Ldr | Lea | Not | Ret
       | Rti | St  | Sti | Str | Trap
       | Getc | Out | Puts | In | Putsp | Halt
       | Fill | Blkw | Stringz )
       Newlines .

Add     = add RegCommaRegComma ( reg | number ) .
And     = and RegCommaRegComma ( reg | number ) .
Br      = br LabelOrOffset .
Jmp     = jmp reg .
Jsr     = jsr LabelOrOffset .
Jsrr    = jsrr reg .
Ld      = ld RegOffset .
Ldi     = ldi RegOffset .
Ldr     = ldr TwoRegOneImm .
Lea     = lea RegOffset .
Not     = not RegCommaReg .
Ret     = ret .
Rti     = rti .
St      = st RegOffset .
Sti     = sti RegOffset .
Str     = str TwoRegOneImm .
Trap    = trap number .
Getc    = getc .
Out     = out .
Puts    = puts .
In      = in .
Putsp   = putsp .
Halt    = halt .
Fill    = fill number .
Blkw    = blkw number .
Stringz = stringz string .
```

### Other

```
Newlines         = newline MoreNewlines .
MoreNewlines     = { newline } .
RegCommaRegComma = RegCommaReg [ comma ] .
RegCommaReg      = Reg [ comma ] Reg .
Reg              = reg .
LabelOrOffset    = label | number .
RegOffset        = Reg [ comma ] LabelOrOffset .
TwoRegOneImm     = RegCommaRegComma number .
```

This non-terminals help parse and reuse code.
