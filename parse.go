package assembler

import (
	"fmt"
)

func parse(src []byte) ([]*segment, error) {
	p := &parser{}

	// tokenize
	tokens, err := tokenize(src)
	if err != nil {
		return nil, err
	}
	p.tokens = tokens

	// parse
	seg := p.SourceFile()
	if p.err != nil {
		return nil, err
	}

	// calculate offset of labels
	for _, s := range seg {
		for _, index := range s.todo {
			i := s.inst[index]
			if target, ok := s.labels[i.label]; !ok {
				return nil, fmt.Errorf("unknown label %s", i.label)
			} else {
				diff := uint16(target - (index + 1))
				if !isLegalOffset(diff, i.width) {
					return nil, fmt.Errorf("label offset out of range %s", i.label)
				}
				i.code |= diff & (1<<i.width - 1)
			}
		}
	}
	return seg, nil
}

func isLegalOffset(x uint16, width uint16) bool {
	var mask uint16 = ^(1<<(width-1) - 1)
	return x&mask == mask || x&mask == 0
}

const (
	mAdd    uint16 = 0b0001_000_000_0_00_000
	mAddImm uint16 = 0b0001_000_000_1_00000
	mAnd    uint16 = 0b0101_000_000_0_00_000
	mAndImm uint16 = 0b0101_000_000_1_00000
	mBr     uint16 = 0b0000_000_000000000
	mJmp    uint16 = 0b1100_0_00000000000
	mJsr    uint16 = 0b0100_1_00000000000
	mJsrr   uint16 = 0b0100_0_00_000_000000
	mLd     uint16 = 0b0010_000_000000000
	mLdi    uint16 = 0b1010_000_000000000
	mLdr    uint16 = 0b0110_000_000_000000
	mLea    uint16 = 0b1110_000_000000000
	mNot    uint16 = 0b1001_000_000_111111
	mRet    uint16 = 0b1100_000_111_000000
	mRti    uint16 = 0b1000_000000000000
	mSt     uint16 = 0b0011_000_000000000
	mSti    uint16 = 0b1011_000_000_000000
	mStr    uint16 = 0b0111_000_000_000000
	mTrap   uint16 = 0b1111_0000_00000000
	mGetc   uint16 = 0b1111_0000_0010_0000
	mOut    uint16 = 0b1111_0000_0010_0001
	mPuts   uint16 = 0b1111_0000_0010_0010
	mIn     uint16 = 0b1111_0000_0010_0011
	mPutsp  uint16 = 0b1111_0000_0010_0100
	mHalt   uint16 = 0b1111_0000_0010_0101
)

type inst struct {
	label string
	code  uint16
	width uint16 // offset width, for later check
}

type segment struct {
	start  uint16
	labels map[string]int // offset has sign
	inst   []*inst
	todo   []int // which has label
}

type parser struct {
	tokens []*token
	index  int
	err    error
}

// peek and pop can read token from token array.
// p.index must be less than len(p.tokens) because of end_of_file
func (p *parser) peek() *token { return p.tokens[p.index] }
func (p *parser) pop() *token  { t := p.peek(); p.index++; return t }

// expectToken updates index if next token's kind is what expected.
func (p *parser) expectToken(kind tokenType) (bool, *token) {
	if t := p.peek(); t != nil && t.kind == kind {
		p.pop()
		return true, t
	} else {
		return false, t
	}
}

// SourceFile = MoreNewlines [ Segment { newline MoreNewlines Segment } MoreNewlines ] .
func (p *parser) SourceFile() []*segment {
	// MoreNewlines
	p.MoreNewlines()

	// [ Segment { newline MoreNewlines Segment } MoreNewlines ]
	var segments []*segment
	if seg := p.Segment(); seg != nil {
		for seg != nil {
			segments = append(segments, seg)
			if p.Newlines(); p.err != nil {
				return nil
			}
			if seg = p.Segment(); p.err != nil {
				return nil
			}
		}
	} else if p.err != nil {
		return nil
	}
	return segments
}

// Segment = Orig { Inst } end .
func (p *parser) Segment() *segment {
	// Orig
	seg := &segment{labels: map[string]int{}}
	if ok, start := p.Orig(); !ok {
		return nil
	} else {
		seg.start = start
	}

	// { Inst }
	for {
		if label, is := p.Inst(); is == nil {
			if p.err != nil {
				return nil
			}
			break
		} else {
			if label != "" {
				seg.labels[label] = len(seg.inst)
			}
			if is[0].label != "" {
				seg.todo = append(seg.todo, len(seg.inst))
			}
			seg.inst = append(seg.inst, is...)
		}
	}

	// end
	if ok, t := p.expectToken(tEnd); !ok {
		p.err = fmt.Errorf("segment should end with \".end\" instead of %s", t.raw)
		return nil
	}
	return seg
}

// Orig = orig number Newlines .
func (p *parser) Orig() (bool, uint16) {
	// ".orig"
	if ok, t := p.expectToken(tOrig); !ok {
		if t.kind != tEndOfFile {
			p.err = fmt.Errorf("segment should end with \".orig\" instead of %s", t.raw)
		}
		return false, 0
	}

	// number
	var start uint16
	if ok, t := p.expectToken(tNumber); !ok {
		p.err = fmt.Errorf("\".orig\" should be followed by a number instead of %s", t.raw)
		return false, 0
	} else {
		start = t.val
	}

	// Newlines
	if p.Newlines(); p.err != nil {
		return false, 0
	}
	return true, start
}

// Inst = [ label MoreNewlines ]
//        ( Add | And | Br  | Jmp | Jsr | Jsrr
//        | Ld  | Ldi | Ldr | Lea | Not | Ret
//        | Rti | St  | Sti | Str | Trap
//        | Getc | Out | Puts | In | Putsp | Halt
//        | Fill | Blkw | Stringz )
//        Newlines .
func (p *parser) Inst() (string, []*inst) {
	// with label
	if ok, t := p.expectToken(tLabel); ok {
		p.MoreNewlines()
		is := p.BareInst()
		if p.err != nil {
			return "", nil
		}
		if is == nil {
			p.err = fmt.Errorf("label %s should be followed by an instruction"+
				" (except \".orig\" and \".end\")", t.raw)
			return "", nil
		}
		if p.Newlines(); p.err != nil {
			return "", nil
		}
		return t.str, is
	}

	// without label
	is := p.BareInst()
	if p.err != nil {
		return "", nil
	}
	if is == nil {
		return "", nil
	}
	if p.Newlines(); p.err != nil {
		return "", nil
	}
	return "", is
}
func (p *parser) BareInst() []*inst {
	functions := []func() []*inst{
		p.Add, p.And, p.Br, p.Jmp, p.Jsr, p.Jsrr, p.Ld, p.Ldi, p.Ldr, p.Lea, p.Not, p.Ret, p.Rti, p.St,
		p.Str, p.Sti, p.Trap, p.Getc, p.Out, p.Puts, p.In, p.Putsp, p.Halt, p.Fill, p.Blkw, p.Stringz,
	}
	for _, f := range functions {
		if is := f(); p.err != nil {
			return nil
		} else if len(is) > 0 {
			return is
		}
	}
	return nil
}

// Add = add RegCommaRegComma ( reg | number ) .
func (p *parser) Add() []*inst {
	// add
	if ok, _ := p.expectToken(tAdd); !ok {
		return nil
	}

	// RegCommaRegComma
	i := &inst{}
	if r0, r1 := p.RegCommaRegComma(); p.err != nil {
		p.err = fmt.Errorf("add: %v", p.err)
		return nil
	} else {
		i.code |= r0<<9 | r1<<6
	}

	// ( reg | number )
	if ok, t := p.expectToken(tReg); ok {
		i.code |= mAdd | t.val
	} else if ok, t = p.expectToken(tNumber); ok {
		i.code |= mAddImm | t.val&(1<<5-1)
	} else {
		p.err = fmt.Errorf("add: expect register or number but met %s", t.raw)
		return nil
	}
	return []*inst{i}
}

// And = and RegCommaRegComma ( reg | number ) .
func (p *parser) And() []*inst {
	// and
	if ok, _ := p.expectToken(tAnd); !ok {
		return nil
	}

	// RegCommaRegComma
	i := &inst{}
	if r0, r1 := p.RegCommaRegComma(); p.err != nil {
		p.err = fmt.Errorf("and: instuction, %v", p.err)
		return nil
	} else {
		i.code |= r0<<9 | r1<<6
	}

	// ( reg | number )
	if ok, t := p.expectToken(tReg); ok {
		i.code |= mAnd | t.val
	} else if ok, t = p.expectToken(tNumber); ok {
		i.code |= mAndImm | t.val&(1<<5-1)
	} else {
		p.err = fmt.Errorf("and: expect register or number but met %s", t.raw)
		return nil
	}
	return []*inst{i}
}

// Br = br LabelOrOffset .
func (p *parser) Br() []*inst {
	// br
	i := &inst{code: mBr}
	if ok, t := p.expectToken(tBr); !ok {
		return nil
	} else {
		i.code |= t.val << 9
	}

	// LabelOrOffset
	if label, offset := p.LabelOrOffset(); p.err != nil {
		return nil
	} else {
		if label != "" {
			i.label = label
			i.width = 9
		} else {
			if !isLegalOffset(offset, 9) {
				p.err = fmt.Errorf("br: offset out of range %v", offset)
				return nil
			}
			i.code |= offset & (1<<9 - 1)
		}
		return []*inst{i}
	}
}

// Jmp = jmp reg .
func (p *parser) Jmp() []*inst {
	// jmp
	if ok, _ := p.expectToken(tJmp); !ok {
		return nil
	}

	// reg
	if ok, t := p.expectToken(tReg); !ok {
		p.err = fmt.Errorf("jmp: expect register but met %s", t.raw)
		return nil
	} else {
		return []*inst{{code: mJmp | t.val<<6}}
	}
}

// Jsr = jsr LabelOrOffset .
func (p *parser) Jsr() []*inst {
	// jsr
	if ok, _ := p.expectToken(tJsr); !ok {
		return nil
	}

	// LabelOrOffset
	if label, offset := p.LabelOrOffset(); p.err != nil {
		return nil
	} else {
		if label != "" {
			return []*inst{{code: mJsr, label: label, width: 11}}
		} else {
			if !isLegalOffset(offset, 11) {
				p.err = fmt.Errorf("jsr: offset out of range %v", offset)
				return nil
			}
			return []*inst{{code: mJsr | offset&(1<<11-1)}}
		}
	}
}

// Jsrr = jsrr reg .
func (p *parser) Jsrr() []*inst {
	// jsrr
	if ok, _ := p.expectToken(tJsrr); !ok {
		return nil
	}

	// reg
	if ok, t := p.expectToken(tReg); !ok {
		p.err = fmt.Errorf("jsrr: expect register but met %s", t.raw)
		return nil
	} else {
		return []*inst{{code: mJsrr | t.val<<6}}
	}
}

// Ld = ld RegOffset .
func (p *parser) Ld() []*inst {
	// ld
	if ok, _ := p.expectToken(tLd); !ok {
		return nil
	}

	// RegOffset
	if r, label, offset := p.RegOffset(); p.err != nil {
		p.err = fmt.Errorf("ld: %v", p.err)
		return nil
	} else {
		if label != "" {
			return []*inst{{code: mLd | r<<9, label: label, width: 9}}
		} else {
			if !isLegalOffset(offset, 9) {
				p.err = fmt.Errorf("ld: offset out of range %v", offset)
				return nil
			}
			return []*inst{{code: mLd | r<<9 | offset&(1<<9-1)}}
		}
	}
}

// Ldi = ldi RegOffset .
func (p *parser) Ldi() []*inst {
	// ldi
	if ok, _ := p.expectToken(tLdi); !ok {
		return nil
	}

	// RegOffset
	if r, label, offset := p.RegOffset(); p.err != nil {
		p.err = fmt.Errorf("ldi: %v", p.err)
		return nil
	} else {
		if label != "" {
			return []*inst{{code: mLdi | r<<9, label: label, width: 9}}
		} else {
			if !isLegalOffset(offset, 9) {
				p.err = fmt.Errorf("ldi: offset out of range %v", offset)
				return nil
			}
			return []*inst{{code: mLdi | r<<9 | offset&(1<<9-1)}}
		}
	}
}

// Ldr = ldr TwoRegOneImm .
func (p *parser) Ldr() []*inst {
	// ldr
	if ok, _ := p.expectToken(tLdr); !ok {
		return nil
	}

	// TwoRegOneImm
	if r0, r1, imm := p.TwoRegOneImm(); p.err != nil {
		p.err = fmt.Errorf("ldr: %v", p.err)
		return nil
	} else {
		if !isLegalOffset(imm, 6) {
			p.err = fmt.Errorf("ldr: offset out of range %v", imm)
			return nil
		}
		return []*inst{{code: mLdr | r0<<9 | r1<<6 | imm&(1<<6-1)}}
	}
}

// Lea = lea RegOffset .
func (p *parser) Lea() []*inst {
	// lea
	if ok, _ := p.expectToken(tLea); !ok {
		return nil
	}

	// RegOffset
	if r, label, offset := p.RegOffset(); p.err != nil {
		p.err = fmt.Errorf("lea: %v", p.err)
		return nil
	} else {
		if label != "" {
			return []*inst{{code: mLea | r<<9, label: label, width: 9}}
		} else {
			if !isLegalOffset(offset, 9) {
				p.err = fmt.Errorf("lea: offset out of range %v", offset)
				return nil
			}
			return []*inst{{code: mLea | r<<9 | offset&(1<<9-1)}}
		}
	}
}

// Not = not RegCommaReg .
func (p *parser) Not() []*inst {
	// not
	if ok, _ := p.expectToken(tNot); !ok {
		return nil
	}

	// RegCommaReg
	if r0, r1 := p.RegCommaReg(); p.err != nil {
		p.err = fmt.Errorf("not: %v", p.err)
		return nil
	} else {
		return []*inst{{code: mNot | r0<<9 | r1<<6}}
	}
}

// Ret = ret .
func (p *parser) Ret() []*inst {
	if ok, _ := p.expectToken(tRet); ok {
		return []*inst{{code: mRet}}
	}
	return nil
}

// Rti = rti .
func (p *parser) Rti() []*inst {
	if ok, _ := p.expectToken(tRti); ok {
		return []*inst{{code: mRti}}
	}
	return nil
}

// St = st RegOffset .
func (p *parser) St() []*inst {
	// st
	if ok, _ := p.expectToken(tSt); !ok {
		return nil
	}

	// RegOffset
	if r, label, offset := p.RegOffset(); p.err != nil {
		p.err = fmt.Errorf("st: %v", p.err)
		return nil
	} else {
		if label != "" {
			return []*inst{{code: mSt | r<<9, label: label, width: 9}}
		} else {
			if !isLegalOffset(offset, 9) {
				p.err = fmt.Errorf("st: offset out of range %v", offset)
				return nil
			}
			return []*inst{{code: mSt | r<<9 | offset&(1<<9-1)}}
		}
	}
}

// Sti = sti RegOffset .
func (p *parser) Sti() []*inst {
	// st
	if ok, _ := p.expectToken(tSti); !ok {
		return nil
	}

	// RegOffset
	if r, label, offset := p.RegOffset(); p.err != nil {
		p.err = fmt.Errorf("sti: %v", p.err)
		return nil
	} else {
		if label != "" {
			return []*inst{{code: mSti | r<<9, label: label, width: 9}}
		} else {
			if !isLegalOffset(offset, 9) {
				p.err = fmt.Errorf("sti: offset out of range %v", offset)
				return nil
			}
			return []*inst{{code: mSti | r<<9 | offset&(1<<9-1)}}
		}
	}
}

// Str = str TwoRegOneImm .
func (p *parser) Str() []*inst {
	// str
	if ok, _ := p.expectToken(tStr); !ok {
		return nil
	}

	// TwoRegOneImm
	if r0, r1, imm := p.TwoRegOneImm(); p.err != nil {
		p.err = fmt.Errorf("str: %v", p.err)
		return nil
	} else {
		if !isLegalOffset(imm, 6) {
			p.err = fmt.Errorf("str: offset out of range %v", imm)
			return nil
		}
		return []*inst{{code: mStr | r0<<9 | r1<<6 | imm&(1<<6-1)}}
	}
}

// Trap = trap number .
func (p *parser) Trap() []*inst {
	// trap
	if ok, _ := p.expectToken(tTrap); !ok {
		return nil
	}

	if ok, t := p.expectToken(tNumber); !ok {
		p.err = fmt.Errorf("trap: expect number but met %s", t.raw)
		return nil
	} else {
		if t.val > 0xff {
			p.err = fmt.Errorf("trap: vector out of range %v", t.val)
			return nil
		}
		return []*inst{{code: mTrap | t.val&(1<<8-1)}}
	}
}

// Getc = getc .
func (p *parser) Getc() []*inst {
	if ok, _ := p.expectToken(tGetc); ok {
		return []*inst{{code: mGetc}}
	}
	return nil
}

// Out = out .
func (p *parser) Out() []*inst {
	if ok, _ := p.expectToken(tOut); ok {
		return []*inst{{code: mOut}}
	}
	return nil
}

// Puts = puts .
func (p *parser) Puts() []*inst {
	if ok, _ := p.expectToken(tPuts); ok {
		return []*inst{{code: mPuts}}
	}
	return nil
}

// In = in .
func (p *parser) In() []*inst {
	if ok, _ := p.expectToken(tIn); ok {
		return []*inst{{code: mIn}}
	}
	return nil
}

// Putsp = putsp .
func (p *parser) Putsp() []*inst {
	if ok, _ := p.expectToken(tPutsp); ok {
		return []*inst{{code: mPutsp}}
	}
	return nil
}

// Halt = halt .
func (p *parser) Halt() []*inst {
	if ok, _ := p.expectToken(tHalt); ok {
		return []*inst{{code: mHalt}}
	}
	return nil
}

// Fill = fill number .
func (p *parser) Fill() []*inst {
	if ok, _ := p.expectToken(tFill); !ok {
		return nil
	}

	if ok, t := p.expectToken(tNumber); !ok {
		p.err = fmt.Errorf("\".fill\" should be followed by a number instead of %s", t.raw)
		return nil
	} else {
		return []*inst{{code: t.val}}
	}
}

// Blkw = blkw number .
func (p *parser) Blkw() []*inst {
	if ok, _ := p.expectToken(tBlkw); !ok {
		return nil
	}

	if ok, t := p.expectToken(tNumber); !ok {
		p.err = fmt.Errorf("\".blkw\" should be followed by a number instead of %s", t.raw)
		return nil
	} else {
		is := make([]*inst, t.val)
		for i := range is {
			is[i] = &inst{}
		}
		return is
	}
}

// Stringz = stringz string .
func (p *parser) Stringz() []*inst {
	if ok, _ := p.expectToken(tStringz); !ok {
		return nil
	}

	if ok, t := p.expectToken(tString); !ok {
		p.err = fmt.Errorf("\".stringz\" should be followed by a string instead of %s", t.raw)
		return nil
	} else {
		is := make([]*inst, 0, len(t.str)+1)
		for i := 0; i < len(t.str); i++ {
			is = append(is, &inst{code: uint16(t.str[i])})
		}
		is = append(is, &inst{})
		return is
	}
}

// Newlines = newline MoreNewlines .
func (p *parser) Newlines() {
	if ok, _ := p.expectToken(tNewline); !ok {
		p.err = fmt.Errorf("only one instruction can be written per line")
	}
	p.MoreNewlines()
}

// MoreNewlines = { newline } .
func (p *parser) MoreNewlines() {
	for ok, _ := p.expectToken(tNewline); ok; ok, _ = p.expectToken(tNewline) {
	}
}

// RegCommaRegComma = Reg [ comma ] Reg [ comma ] .
func (p *parser) RegCommaRegComma() (uint16, uint16) {
	var r0, r1 uint16
	if r0, r1 = p.RegCommaReg(); p.err != nil {
		return 0, 0
	}
	p.expectToken(tComma)
	return r0, r1
}

// RegCommaReg = Reg [ comma ] Reg .
func (p *parser) RegCommaReg() (uint16, uint16) {
	var r0, r1 uint16
	if r0 = p.Reg(); p.err != nil {
		return 0, 0
	}
	p.expectToken(tComma)
	if r1 = p.Reg(); p.err != nil {
		return 0, 0
	}
	return r0, r1
}

// Reg = reg .
func (p *parser) Reg() uint16 {
	if ok, t := p.expectToken(tReg); !ok {
		p.err = fmt.Errorf("expect register but met %s", t.raw)
		return 0
	} else {
		return t.val
	}
}

// LabelOrOffset = label | number .
func (p *parser) LabelOrOffset() (string, uint16) {
	if ok, t := p.expectToken(tLabel); ok {
		return t.str, 0
	} else if ok, t = p.expectToken(tNumber); ok {
		return "", t.val
	} else {
		p.err = fmt.Errorf("expect label or number but met %s", t.raw)
		return "", 0
	}
}

// RegOffset = Reg [ comma ] ( label | number ) .
func (p *parser) RegOffset() (r uint16, label string, offset uint16) {
	if r = p.Reg(); p.err != nil {
		return
	}
	p.expectToken(tComma)
	if ok, t := p.expectToken(tLabel); ok {
		label = t.str
	} else if ok, t = p.expectToken(tNumber); ok {
		offset = t.val
	} else {
		p.err = fmt.Errorf("expect label for number but met %s", t.raw)
	}
	return
}

// TwoRegOneImm = RegCommaRegComma number
func (p *parser) TwoRegOneImm() (r0, r1, imm uint16) {
	if r0, r1 = p.RegCommaRegComma(); p.err != nil {
		return
	}
	if ok, t := p.expectToken(tNumber); !ok {
		p.err = fmt.Errorf("expect number but met %s", t.raw)
	} else {
		imm = t.val
	}
	return
}
