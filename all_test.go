package assembler

import (
	"fmt"
	"testing"
)

func TestTokenize(t *testing.T) {
	tokens, err := tokenize([]byte(`
        .ORIG x3000 ; R2 = R0 * R1
        AND R2, R2, #0
        ADD R0, R0, #0
LOOP    BRz END
        ADD R2, R2, R1
        ADD R0, R0, #-1
        BR LOOP
END     TRAP x25
        .END
	`))
	fmt.Println(tokens, err)
}

func TestCaseIdCheck(t *testing.T) {
	seg, _ := Assemble([]byte(
		`.ORIG x3000

        ; print prompt
        LEA R0, prompt
        PUTS

        ; read input
        LEA R1, data

INPUT   GETC
        OUT
        
        ADD R2, R0, #-10
        BRz NEXT
        ADD R2, R0, #-13
        BRz NEXT

        STR R0, R1, #0
        ADD R1, R1, #1

        BR INPUT

        ; calculate
NEXT    LEA R2, data
        LEA R3, weight
        AND R4, R4, #0
        AND R5, R5, #0
        LD R6, ascii_0

CAL     ADD R0, R2, R4
        LDR R0, R0, #0
        ADD R0, R0, R6

        ADD R1, R3, R4
        LDR R1, R1, #0

        JSR MUL

        ADD R5, R5, R0
        ADD R4, R4, #1
        ADD R0, R4, #-10
        ADD R0, R0, #-7
        BRn CAL

        ADD R0, R5, #0
        JSR MOD

        NOT R0, R0
        ADD R0, R0, #1
        ADD R0, R0, #12
        JSR MOD

        ; compare
        ADD R1, R2, R4
        LDR R1, R1, #0

        ADD R2, R0, #-10
        BRz IS_X

        ADD R1, R1, R6
        NOT R1, R1
        ADD R1, R1, #1
        ADD R0, R0, R1
        BRz RIGHT
        BR WRONG

IS_X    LD R0, ascii_x
        ADD R0, R1, R0
        BRz RIGHT
        BR WRONG

RIGHT   LEA R0, success
        BR END
WRONG   LEA R0, fail

END     PUTS
        HALT

        prompt  .STRINGZ "enter ID number: "
        success .STRINGZ "The ID number's check digit is correct.\n"
        fail    .STRINGZ "The ID number's check digit is incorrect.\n"
        data    .BLKW #18
        weight
                .FILL #7
                .FILL #9
                .FILL #10
                .FILL #5
                .FILL #8
                .FILL #4
                .FILL #2
                .FILL #1
                .FILL #6
                .FILL #3
                .FILL #7
                .FILL #9
                .FILL #10
                .FILL #5
                .FILL #8
                .FILL #4
                .FILL #2
        ascii_0 .FILL #-48
        ascii_x .FILL #-88

MUL     ST R3, SAVE_R3
        AND R3, R3, #0

MUL_LOOP
        ADD R0, R0, #0
        BRz MUL_END
        ADD R3, R3, R1
        ADD R0, R0, #-1
        BR MUL_LOOP

MUL_END 
        ADD R0, R3, #0
        LD R3, SAVE_R3
        RET
        SAVE_R3 .FILL #0

MOD     ADD R0, R0, #-11
        BRp MOD
        BRz MOD_END
        ADD R0, R0, #11
MOD_END RET

.END
`))
	fmt.Println(seg[0].ToString(Addr | Hex))
}

func TestCaseSki(t *testing.T) {
	seg, _ := Assemble([]byte(
		`        .ORIG   x3000

; Initialize
        LD      R6, StackBase   ; Use R6 as stack pointer
        AND     R2, R2, #0      ; Use R2 to store the answer
        AND     R5, R5, #0      ; Use R5 to store the number of steps
        ADD     R5, R5, #1

        LDI     R3, Row
LoopI   ADD     R3, R3, #-1     ; R3 from Row-1 to 0
        BRn     BreakI
        LDI     R4, Column
LoopJ   ADD     R4, R4, #-1     ; R4 from Column-1 to 0
        BRn     BreakJ
        JSR     DFS             ; DFS(R3, R4, 1)
        BR      LoopJ
BreakJ  BR      LoopI
BreakI  HALT

; DFS(R3, R4, R5) : point (R3, R4), step = R5
DFS     ADD     R6, R6, #-1     ; Push R7 and R1 into the stack
        STR     R7, R6, #0
        ADD     R6, R6, #-1
        STR     R1, R6, #0
        
        NOT     R1, R2          
        ADD     R1, R1, #1
        ADD     R1, R1, R5
        BRnz    PassAns         ; If steps > answer
        ADD     R2, R5, #0      ; answer = steps
PassAns JSR     DATA
        NOT     R1, R0          ; For comparation, 
        ADD     R1, R1, #1      ; R1 = - Data[R3][R4]

        ADD     R5, R5, #1

        ADD     R3, R3, #1      ; Judge point (R3+1, R4)
        LDI     R0, Row
        NOT     R0, R0
        ADD     R0, R0, #1
        ADD     R0, R0, R3
        BRz     Pass1           ; If R3+1 < Row
        JSR     DATA
        ADD     R0, R0, R1
        BRzp    Pass1           ; If Data[R3+1][R4] < Data[R3][R4]
        JSR     DFS             ; Then DFS(R3+1, R4, step+1)
Pass1   ADD     R3, R3, #-1     ; Reset R3

        ADD     R3, R3, #-1     ; Judge point (R3-1, R4)
        BRn     Pass2           ; If R3-1 >= 0
        JSR     DATA
        ADD     R0, R0, R1
        BRzp    Pass2           ; If Data[R3-1][R4] < Data[R3][R4]
        JSR     DFS             ; Then DFS(R3-1, R4, step+1)
Pass2   ADD     R3, R3, #1      ; Reset R3

        ADD     R4, R4, #1      ; Judge point (R3, R4+1)
        LDI     R0, Column
        NOT     R0, R0
        ADD     R0, R0, #1
        ADD     R0, R0, R4
        BRz     Pass3
        JSR     DATA
        ADD     R0, R0, R1
        BRzp    Pass3
        JSR     DFS
Pass3   ADD     R4, R4, #-1

        ADD     R4, R4, #-1     ; Judge point (R3, R4-1)
        BRn     Pass4
        JSR     DATA
        ADD     R0, R0, R1
        BRzp    Pass4
        JSR     DFS
Pass4   ADD     R4, R4, #1
        
        ADD     R5, R5, #-1     ; Reset step

        LDR     R1, R6, #0      ; Pop R1 and R7 from the stack
        ADD     R6, R6, #1
        LDR     R7, R6, #0
        ADD     R6, R6, #1
        RET

; DATA(R3, R4) : R0 = Data[R3][R4]
DATA    ST      R1, S1
        ST      R2, S2

        LD      R0, DataBase
        LDI     R2, Column

        ADD     R1, R3, #0      ; addr = R3 * column + R4 + x3202
LoopD1  BRz     BreakD1
        ADD     R0, R0, R2
        ADD     R1, R1, #-1
        BR      LoopD1
BreakD1 ADD     R1, R4, #0
LoopD2  BRz     BreakD2
        ADD     R0, R0, #1
        ADD     R1, R1, #-1
        BR      LoopD2
BreakD2 LDR     R0, R0, #0

        LD      R1, S1
        LD      R2, S2
        RET

Row     .FILL   x3200
Column  .FILL   x3201
DataBase .FILL  x3202
StackBase .FILL x3000
S1      .BLKW   #1
S2      .BLKW   #1
        .END

`))
	fmt.Println(seg[0].ToString(Addr | Hex))
}
